group:
  activity:
    name: Activity Tracker
    entities:
      - input_select.martin_activity
      - input_select.latest_motion
      - input_boolean.guests

input_boolean:
  guests:
    name: Got Guests
    initial: off
    icon: mdi:account-multiple


input_select:
  martin_activity:
    name: Martin Activity
    options:
      - Unknown
      - Away
      - Watching TV
      - In the bathroom
      - In the kitchen
      - Showering # Consider making this a separate binary sensor on the bathroom?
      - Sleeping
      - Sleep Paused
      - Waking
      - Awake

  # TODO: Hide this in the UI.
  martin_previous_activity:
    name: Martin Previous Activity
    options:
      - Unknown

  latest_motion:
    name: Latest Motion
    options:
      - Unknown
      - Away
      - Hallway
      - Kitchen
      - Bathroom
      - Bedroom
      - Livingroom
      - Office

  previous_motion:
    name: Previous Motion
    options:
      - Unknown

binary_sensor:
  - platform: template
    sensors:
      activity_last10:
        value_template: "{{ is_state('group.activity_sensors', 'on') }}"
        friendly_name: Activity in Last 10 min
        device_class: motion
        delay_off:
          minutes: 10
      activity_last30:
        value_template: "{{ is_state('group.activity_sensors', 'on') }}"
        friendly_name: Activity in Last 30 min
        device_class: motion
        delay_off:
          minutes: 30


script:
  return_to_previous_activity:
    alias: "Return to previous activity"
    sequence:
      service: input_select.select_option
      data:
        entity_id: input_select.martin_activity
        option: "{{ states('input_select.martin_previous_activity') }}"

automation:
  - alias: latest_motion_kitchen
    trigger:
      platform: state
      entity_id: binary_sensor.kitchen_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Kitchen'

  - alias: latest_motion_bathroom
    trigger:
      platform: state
      entity_id: binary_sensor.toilet_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Bathroom'

  - alias: latest_motion_bedroom
    trigger:
      platform: state
      entity_id: binary_sensor.bedroom_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Bedroom'

  - alias: latest_motion_hallway
    trigger:
      platform: state
      entity_id: binary_sensor.hallway_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Hallway'

  - alias: latest_motion_hallway_door
    trigger:
      platform: state
      entity_id: binary_sensor.apartment_door
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Hallway'

  - alias: latest_motion_livingroom
    trigger:
      platform: state
      entity_id: binary_sensor.livingroom_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Livingroom'

  - alias: latest_motion_office
    trigger:
      platform: state
      entity_id: binary_sensor.office_sensor
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Office'

  - alias: latest_motion_driving
    trigger:
      platform: state
      entity_id: binary_sensor.driving
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.latest_motion
        option: 'Away'

  - alias: update_previous_motion
    trigger:
      platform: state
      entity_id: input_select.latest_motion
    action:
      service: input_select.set_options
      data_template:
        entity_id: input_select.previous_motion
        options: "{{ trigger.from_state.state }}"

  #####################
  # Activity Triggers #
  #####################
  - alias: update_previous_activity
    trigger:
      platform: state
      entity_id: input_select.martin_activity
    condition:
      condition: state
      entity_id: input_boolean.guests
      state: 'off'
    action:
      service: input_select.set_options
      data_template:
        entity_id: input_select.martin_previous_activity
        options: "{{ trigger.from_state.state }}"

  - alias: driving_activity
    trigger:
      platform: state
      entity_id: binary_sensor.driving
      to: 'on'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.martin_activity
        option: 'Away'

  - alias: bathroom_activity
    trigger:
      platform: state
      entity_id: input_select.latest_motion
      to: 'Bathroom'
    condition:
      condition: state
      entity_id: input_boolean.guests
      state: 'off'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.martin_activity
        option: In the bathroom

  - alias: shower_activity
    trigger:
      platform: numeric_state
      entity_id: sensor.toilet_relative_humidity_change
      above: 5
    condition:
      condition: and
      conditions:
        - condition: state
          entity_id: input_boolean.guests
          state: 'off'
        - condition: state
          entity_id: input_select.latest_motion
          state: 'Bathroom'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.martin_activity
        option: Showering

  - alias: kitchen_activity
    trigger:
      platform: state
      entity_id: input_select.latest_motion
      to: 'Kitchen'
    condition:
      condition: state
      entity_id: input_boolean.guests
      state: 'off'
    action:
      service: input_select.select_option
      data:
        entity_id: input_select.martin_activity
        option: In the kitchen

  ##########################
  # Activity based actions #
  ##########################

  - alias: pause_tv_chromecast_on_activity_change
    trigger:
      platform: state
      entity_id: input_select.martin_activity
      from: Watching TV
    condition:
        condition: state
        entity_id: media_player.tv
        state: 'playing'
    action:
      service: media_player.media_pause
      data:
        entity_id: media_player.tv
